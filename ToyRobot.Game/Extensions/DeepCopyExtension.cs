﻿using Newtonsoft.Json;

namespace ToyRobot.Game.Extensions
{
    public static class DeepCopyExtension
    {
        public static T DeepCopy<T>(this T source)
        {
            if (source == null)
            {
                return default;
            }

            var sourceJson = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(sourceJson);
        }
    }
}
