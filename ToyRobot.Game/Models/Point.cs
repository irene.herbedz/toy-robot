﻿namespace ToyRobot.Game.Models
{
    public class Point
    {
        public uint X { get; set; }
        public uint Y { get; set; }

        public Point(uint x, uint y)
        {
            X = x;
            Y = y;
        }
    }
}
