﻿using System;
using ToyRobot.Game.Interfaces;

namespace ToyRobot.Game.Models
{
    public class Board : IBoard
    {
        public uint Width { get; }
        public uint Height { get; }

        public Board(uint width, uint height)
        {
            Width = width;
            Height = height;
        }

        public bool IsValidPosition(Point point)
        {
            if (point == null)
            {
                throw new ArgumentNullException(nameof(point));
            }

            return point.X < Width && point.Y < Height;
        }
    }
}
