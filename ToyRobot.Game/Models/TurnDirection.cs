﻿namespace ToyRobot.Game.Models
{
    public enum TurnDirection
    {
        Left = -90,
        Right = 90
    }
}
