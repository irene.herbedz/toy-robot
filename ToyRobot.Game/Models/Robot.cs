﻿using ToyRobot.Game.Exceptions;
using ToyRobot.Game.Extensions;
using ToyRobot.Game.Interfaces;

namespace ToyRobot.Game.Models
{
    public class Robot : IRobot
    {
        private IMovementManager _movementManager;
        private RobotPosition _position;

        public Robot(IMovementManager movementManager)
        {
            _movementManager = movementManager;
        }

        public void Move()
        {
            EnsurePlaced();

            var newRobotPosition = _position.DeepCopy();

            switch (_position.Direction)
            {
                case Direction.North:
                    newRobotPosition.Y += 1;
                    break;
                case Direction.East:
                    newRobotPosition.X += 1;
                    break;
                case Direction.South:
                    newRobotPosition.Y -= 1;
                    break;
                case Direction.West:
                    newRobotPosition.X -= 1;
                    break;
            }

            _movementManager.EnsureCanMove(newRobotPosition);

            _position = newRobotPosition;
        }

        public void Turn(TurnDirection turnDirection)
        {
            EnsurePlaced();

            var diff = (int)_position.Direction + (int)turnDirection + 360;
            _position.Direction = (Direction)(diff % 360);
        }

        public void Place(RobotPosition position)
        {
            _movementManager.EnsureCanMove(position);

            _position = position.DeepCopy();
        }

        public RobotPosition Report()
        {
            EnsurePlaced();

            return _position.DeepCopy();
        }

        private void EnsurePlaced()
        {
            if (_position == null)
            {
                throw new RobotNotPlacedException();
            }
        }
    }
}
