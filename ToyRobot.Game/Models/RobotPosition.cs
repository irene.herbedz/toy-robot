﻿using System;

namespace ToyRobot.Game.Models
{
    public class RobotPosition : Point
    {
        public Direction Direction { get; set; }

        public RobotPosition(uint x, uint y, Direction direction) : base(x, y)
        {
            if (!Enum.IsDefined(typeof(Direction), direction))
            {
                throw new ArgumentException(nameof(direction));
            }

            Direction = direction;
        }

        public override string ToString()
        {
            return $"{X}, {Y}, {Direction}";
        }
    }
}
