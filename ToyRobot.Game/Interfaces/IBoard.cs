﻿using ToyRobot.Game.Models;

namespace ToyRobot.Game.Interfaces
{
    public interface IBoard
    {
        uint Width { get; }
        uint Height { get; }

        bool IsValidPosition(Point point);
    }
}
