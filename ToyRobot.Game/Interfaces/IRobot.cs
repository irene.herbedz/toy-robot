﻿using ToyRobot.Game.Models;

namespace ToyRobot.Game.Interfaces
{
    public interface IRobot
    {
        void Place(RobotPosition position);
        void Move();
        void Turn(TurnDirection turnDirection);
        RobotPosition Report();
    }
}
