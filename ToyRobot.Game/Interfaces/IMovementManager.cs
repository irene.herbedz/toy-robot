﻿using ToyRobot.Game.Models;

namespace ToyRobot.Game.Interfaces
{
    public interface IMovementManager
    {
        void EnsureCanMove(Point point);
    }
}
