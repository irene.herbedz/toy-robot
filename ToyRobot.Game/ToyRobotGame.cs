﻿using ToyRobot.Game.Exceptions;
using ToyRobot.Game.Interfaces;
using ToyRobot.Game.Models;

namespace ToyRobot.Game
{
    public class ToyRobotGame : IMovementManager
    {
        private IBoard _board;

        public ToyRobotGame(uint boardWidth = 5, uint boardHeight = 5)
        {
            _board = new Board(boardWidth, boardHeight);
        }

        public IRobot CreateRobot()
        {
            return new Robot(this);
        }

        public void EnsureCanMove(Point point)
        {
            if (!_board.IsValidPosition(point))
            {
                throw new RobotCannotMoveException();
            }
        }
    }
}
