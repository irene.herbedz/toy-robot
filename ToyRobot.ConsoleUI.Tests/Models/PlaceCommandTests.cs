﻿using System;
using ToyRobot.ConsoleUI.Models;
using ToyRobot.Game.Models;
using Xunit;

namespace ToyRobot.ConsoleUI.Tests.Models
{
    public class PlaceCommandTests
    {
        [Fact]
        public void PlaceCommand_Create_SetPlaceCommandType()
        {
            var placeCommand = new PlaceCommand(new RobotPosition(1, 2, Direction.North));
            Assert.Equal(CommandType.Place, placeCommand.CommandType);
        }

        [Fact]
        public void PlaceCommand_WhenRobotPositionNull_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new PlaceCommand(null));
        }
    }
}
