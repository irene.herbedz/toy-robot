﻿using System;
using ToyRobot.ConsoleUI.Models;
using Xunit;

namespace ToyRobot.ConsoleUI.Tests.Models
{
    public class CommandTests
    {
        [Fact]
        public void Command_InvalidType_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new Command((CommandType)1000));
        }
    }
}
