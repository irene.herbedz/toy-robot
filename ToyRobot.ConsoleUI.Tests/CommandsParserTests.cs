﻿using ToyRobot.ConsoleUI.Exceptions;
using ToyRobot.ConsoleUI.Models;
using ToyRobot.Game.Models;
using Xunit;

namespace ToyRobot.ConsoleUI.Tests
{
    public class CommandsParserTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("invalid command")]
        [InlineData("     ")]
        [InlineData("place 1,1,ksdjfd")]
        [InlineData("place 1,1,")]
        [InlineData("place 1,,,")]
        public void Parse_WhenInvalidCommand_ThrowsException(string command)
        {
            var commandParser = new CommandsParser();
            Assert.Throws<UnknownCommandException>(() => commandParser.Parse(command));
        }

        [Theory]
        [InlineData("PLace 1,2, North", CommandType.Place)]
        [InlineData(" moVE ", CommandType.Move)]
        [InlineData("LEFt ", CommandType.Left)]
        [InlineData(" rIGHt", CommandType.Right)]
        [InlineData(" ReporT", CommandType.Report)]
        public void Parse_WhenValidCommand_Success(string command, CommandType expectedCommandType)
        {
            var commandParser = new CommandsParser();

            var parsedCommand = commandParser.Parse(command);

            Assert.Equal(expectedCommandType, parsedCommand.CommandType);
        }

        [Fact]
        public void Parse_WhenPlaceCommand_Success()
        {
            var commandParser = new CommandsParser();

            uint p1 = 1;
            uint p2 = 2;
            var direction = Direction.North;

            var parsedCommand = commandParser.Parse("place 1,  2,  nOrth");

            Assert.Equal(CommandType.Place, parsedCommand.CommandType);
            var placeCommand = parsedCommand as PlaceCommand;
            Assert.Equal(p1, placeCommand.Position.X);
            Assert.Equal(p2, placeCommand.Position.Y);
            Assert.Equal(direction, placeCommand.Position.Direction);
        }
    }
}
