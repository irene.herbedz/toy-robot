﻿using Moq;
using ToyRobot.ConsoleUI.Models;
using ToyRobot.Game.Interfaces;
using ToyRobot.Game.Models;
using Xunit;

namespace ToyRobot.ConsoleUI.Tests
{
    public class ProgramTests
    {
        [Fact]
        public void Execute_MoveCommand_RunMove()
        {
            var robotMock = new Mock<IRobot>();
            robotMock.Setup(_ => _.Move())
                     .Verifiable();

            Program.Execute(new Command(CommandType.Move), robotMock.Object);

            robotMock.Verify(robot => robot.Move(), Times.Once());
        }

        [Fact]
        public void Execute_LeftCommand_RunTurn()
        {
            var robotMock = new Mock<IRobot>();
            robotMock.Setup(_ => _.Turn(It.IsAny<TurnDirection>()))
                     .Verifiable();

            Program.Execute(new Command(CommandType.Left), robotMock.Object);

            robotMock.Verify(robot => robot.Turn(TurnDirection.Left), Times.Once());
        }

        [Fact]
        public void Execute_RightCommand_RunTurn()
        {
            var robotMock = new Mock<IRobot>();
            robotMock.Setup(_ => _.Turn(It.IsAny<TurnDirection>()))
                     .Verifiable();

            Program.Execute(new Command(CommandType.Right), robotMock.Object);

            robotMock.Verify(robot => robot.Turn(TurnDirection.Right), Times.Once());
        }

        [Fact]
        public void Execute_PlaceCommand_RunPlace()
        {
            var robotMock = new Mock<IRobot>();
            robotMock.Setup(_ => _.Place(It.IsAny<RobotPosition>()))
                     .Verifiable();

            var placeCommand = new PlaceCommand(new RobotPosition(1, 2, Direction.East));
            Program.Execute(placeCommand, robotMock.Object);

            robotMock.Verify(robot => robot.Place(placeCommand.Position), Times.Once());
        }

        [Fact]
        public void Execute_ReportCommand_RunReport()
        {
            var robotMock = new Mock<IRobot>();
            robotMock.Setup(_ => _.Report())
                     .Verifiable();

            Program.Execute(new Command(CommandType.Report), robotMock.Object);

            robotMock.Verify(robot => robot.Report(), Times.Once());
        }
    }
}
