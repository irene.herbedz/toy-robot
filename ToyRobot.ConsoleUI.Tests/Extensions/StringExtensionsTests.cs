using System;
using ToyRobot.ConsoleUI.Extensions;
using Xunit;

namespace ToyRobot.ConsoleUI.Tests
{
    public class StringExtensions
    {
        [Fact]
        public void Capitalize_WhenNull_ThrowsException()
        {
            string testString = null;
            Assert.Throws<ArgumentNullException>(() => testString.Capitalize());
        }

        [Fact]
        public void Capitalize_WhenCorrectString_Success()
        {
            string testString = "test STrinG";
            string expectedString = "Test string";

            var actualString = testString.Capitalize();

            Assert.Equal(expectedString, actualString);
        }
    }
}
