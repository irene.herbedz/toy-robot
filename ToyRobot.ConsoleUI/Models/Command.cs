﻿using System;

namespace ToyRobot.ConsoleUI.Models
{
    public class Command
    {
        public CommandType CommandType { get; }

        public Command(CommandType commandType)
        {
            if (!Enum.IsDefined(typeof(CommandType), commandType))
            {
                throw new ArgumentException(nameof(commandType));
            }

            CommandType = commandType;
        }
    }
}
