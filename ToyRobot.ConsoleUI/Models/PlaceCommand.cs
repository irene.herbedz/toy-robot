﻿using System;
using ToyRobot.Game.Models;

namespace ToyRobot.ConsoleUI.Models
{
    public class PlaceCommand : Command
    {
        public RobotPosition Position { get; }

        public PlaceCommand(RobotPosition robotPosition) : base(CommandType.Place)
        {
            if (robotPosition == null)
            {
                throw new ArgumentException(nameof(robotPosition));
            }

            Position = robotPosition;
        }
    }
}
