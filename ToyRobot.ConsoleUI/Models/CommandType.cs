﻿namespace ToyRobot.ConsoleUI.Models
{
    public enum CommandType
    {
        Place,
        Move,
        Left,
        Right,
        Report
    }
}
