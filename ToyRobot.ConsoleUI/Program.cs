﻿using System;
using ToyRobot.ConsoleUI.Exceptions;
using ToyRobot.ConsoleUI.Models;
using ToyRobot.Game;
using ToyRobot.Game.Exceptions;
using ToyRobot.Game.Interfaces;
using ToyRobot.Game.Models;

namespace ToyRobot.ConsoleUI
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Toy Robot simulator");

            var toyRobotGame = new ToyRobotGame();
            var robot = toyRobotGame.CreateRobot();
            ICommandsParser commandParser = new CommandsParser();

            while (true)
            {
                var command = Console.ReadLine();
                try
                {
                    var parsedCommand = commandParser.Parse(command);

                    Execute(parsedCommand, robot);
                }
                catch (Exception ex)
                {
                    HandleException(ex);
                }
            }
        }

        public static void Execute(Command command, IRobot robot)
        {
            switch (command.CommandType)
            {
                case CommandType.Move:
                    robot.Move();
                    break;
                case CommandType.Left:
                    robot.Turn(TurnDirection.Left);
                    break;
                case CommandType.Right:
                    robot.Turn(TurnDirection.Right);
                    break;
                case CommandType.Place:
                    robot.Place((command as PlaceCommand).Position);
                    break;
                case CommandType.Report:
                    var robotPosition = robot.Report();
                    Console.WriteLine(robotPosition);
                    break;
                default:
                    throw new CommandNotImplementedException();
            }
        }

        private static void HandleException(Exception ex)
        {
            if (ex is UnknownCommandException)
            {
                Console.WriteLine("unknown command");
                return;
            }
            if (ex is CommandNotImplementedException)
            {
                Console.WriteLine("command not implemented");
                return;
            }
            if (ex is RobotNotPlacedException)
            {
                Console.WriteLine("robot has not been placed");
                return;
            }
            if (ex is RobotCannotMoveException)
            {
                Console.WriteLine("robot can not move");
                return;
            }

            Console.WriteLine("something went wrong, please try again");
        }
    }
}
