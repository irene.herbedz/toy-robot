﻿using System;
using System.Linq;

namespace ToyRobot.ConsoleUI.Extensions
{
    public static class StringExtensions
    {
        public static string Capitalize(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException(nameof(input));
            }

            return input.First().ToString().ToUpper() + input.Substring(1).ToLower();
        }
    }
}
