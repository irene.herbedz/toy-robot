﻿using System;
using System.Text.RegularExpressions;
using ToyRobot.ConsoleUI.Exceptions;
using ToyRobot.ConsoleUI.Extensions;
using ToyRobot.ConsoleUI.Models;
using ToyRobot.Game.Models;

namespace ToyRobot.ConsoleUI
{
    public class CommandsParser : ICommandsParser
    {
        private static readonly string _simpleCmd = "sc";
        private static readonly string _placeCmd = "pc";
        private static readonly string _placeCmdP1 = "p1";
        private static readonly string _placeCmdP2 = "p2";
        private static readonly string _placeCmdP3 = "p3";

        private static readonly string _commandsPattern = $@"((?<{_simpleCmd}>{CommandType.Move}|{CommandType.Report}|{CommandType.Left}|{CommandType.Right})" +
            $@"|((?<{_placeCmd}>{CommandType.Place})[ ]+(?<{_placeCmdP1}>\d+)[ ]*,[ ]*(?<{_placeCmdP2}>\d+)[ ]*,[ ]*(?<{_placeCmdP3}>{Direction.North}|{Direction.East}|{Direction.South}|{Direction.West})))";

        private static readonly Regex _commandMatcher = new Regex(_commandsPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public Command Parse(string command)
        {
            try
            {
                var match = _commandMatcher.Match(command);

                if (!match.Success)
                {
                    throw new Exception();
                }

                if (match.Groups[_simpleCmd].Success)
                {
                    return new Command((CommandType)Enum.Parse(typeof(CommandType), match.Groups[_simpleCmd].Value.Capitalize()));
                }

                return new PlaceCommand(new RobotPosition(
                    Convert.ToUInt32(match.Groups[_placeCmdP1].Value),
                    Convert.ToUInt32(match.Groups[_placeCmdP2].Value),
                    (Direction)Enum.Parse(typeof(Direction), match.Groups[_placeCmdP3].Value.Capitalize()))
                );
            }
            catch
            {
                throw new UnknownCommandException();
            }
        }
    }
}
