﻿using ToyRobot.ConsoleUI.Models;

namespace ToyRobot.ConsoleUI
{
    public interface ICommandsParser
    {
        Command Parse(string command);
    }
}
