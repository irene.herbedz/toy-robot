﻿using ToyRobot.Game.Models;
using Xunit;

namespace ToyRobot.Game.Tests
{
    public class PointTests
    {
        [Fact]
        public void Point_SetXY_Success()
        {
            uint expectedX = 5;
            uint expectedY = 7;

            var testPoint = new Point(expectedX, expectedY);

            Assert.Equal(testPoint.X, expectedX);
            Assert.Equal(testPoint.Y, expectedY);
        }
    }
}
