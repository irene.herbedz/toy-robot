using Moq;
using ToyRobot.Game.Exceptions;
using ToyRobot.Game.Interfaces;
using ToyRobot.Game.Models;
using Xunit;

namespace ToyRobot.Game.Tests
{
    public class RobotTests
    {
        [Theory]
        [InlineData(Direction.North, TurnDirection.Left, Direction.West)]
        [InlineData(Direction.East, TurnDirection.Left, Direction.North)]
        [InlineData(Direction.South, TurnDirection.Left, Direction.East)]
        [InlineData(Direction.West, TurnDirection.Left, Direction.South)]
        [InlineData(Direction.North, TurnDirection.Right, Direction.East)]
        [InlineData(Direction.East, TurnDirection.Right, Direction.South)]
        [InlineData(Direction.South, TurnDirection.Right, Direction.West)]
        [InlineData(Direction.West, TurnDirection.Right, Direction.North)]
        public void Turn_WhenPlaced_Success(Direction initialDirection, TurnDirection turnDirection, Direction expectedDirection)
        {
            var testGame = new ToyRobotGame();
            var testRobot = testGame.CreateRobot();
            testRobot.Place(new RobotPosition(0, 1, initialDirection));
            testRobot.Turn(turnDirection);
            Assert.Equal(testRobot.Report().Direction, expectedDirection);
        }

        [Fact]
        public void Turn_WhenNotPlaced_ThrowsException()
        {
            var testGame = new ToyRobotGame();
            var testRobot = testGame.CreateRobot();

            Assert.Throws<RobotNotPlacedException>(() => testRobot.Turn(TurnDirection.Left));
        }

        [Fact]
        public void Move_WhenNotPlaced_ThrowsException()
        {
            var testGame = new ToyRobotGame();
            var testRobot = testGame.CreateRobot();

            Assert.Throws<RobotNotPlacedException>(() => testRobot.Move());
        }

        [Fact]
        public void Move_WhenEnsureCanMoveThrowsException_ReThrowsException()
        {
            var movementManagerMoq = new Mock<IMovementManager>();
            movementManagerMoq.Setup(_ => _.EnsureCanMove(It.IsAny<Point>()));

            var testRobot = new Robot(movementManagerMoq.Object);
            testRobot.Place(new RobotPosition(1, 1, Direction.East));

            movementManagerMoq.Setup(_ => _.EnsureCanMove(It.IsAny<Point>()))
                              .Throws<RobotCannotMoveException>();

            Assert.Throws<RobotCannotMoveException>(() => testRobot.Move());
        }

        [Theory]
        [InlineData(2, 2, Direction.North, 2, 3)]
        [InlineData(2, 2, Direction.East, 3, 2)]
        [InlineData(2, 2, Direction.South, 2, 1)]
        [InlineData(2, 2, Direction.West, 1, 2)]
        public void Move_WithValidSetup_Success(uint pointX, uint pointY, Direction direction, uint expectedX, uint expectedY)
        {
            var movementManagerMoq = new Mock<IMovementManager>();
            movementManagerMoq.Setup(_ => _.EnsureCanMove(It.IsAny<Point>()));

            var testRobot = new Robot(movementManagerMoq.Object);
            testRobot.Place(new RobotPosition(pointX, pointY, direction));

            testRobot.Move();
            var actualRobotPosition = testRobot.Report();
            Assert.Equal(actualRobotPosition.X, expectedX);
            Assert.Equal(actualRobotPosition.Y, expectedY);
            Assert.Equal(actualRobotPosition.Direction, direction);
        }

        [Fact]
        public void Place_WhenEnsureCanMoveThrowsException_ReThrowsException()
        {
            var movementManagerMoq = new Mock<IMovementManager>();
            movementManagerMoq.Setup(_ => _.EnsureCanMove(It.IsAny<Point>()))
                              .Throws<RobotCannotMoveException>();

            var testRobot = new Robot(movementManagerMoq.Object);


            Assert.Throws<RobotCannotMoveException>(() => testRobot.Place(new RobotPosition(1, 1, Direction.East)));
        }

        [Fact]
        public void Place_WhenSetPosition_MakesDeepCopy()
        {
            var movementManagerMoq = new Mock<IMovementManager>();
            movementManagerMoq.Setup(_ => _.EnsureCanMove(It.IsAny<Point>()));

            var testRobot = new Robot(movementManagerMoq.Object);

            var robotPosition = new RobotPosition(1, 1, Direction.East);
            testRobot.Place(robotPosition);

            var placedRobotPosition = testRobot.Report();

            Assert.Equal(placedRobotPosition.X, robotPosition.X);
            Assert.Equal(placedRobotPosition.Y, robotPosition.Y);
            Assert.Equal(placedRobotPosition.Direction, robotPosition.Direction);

            robotPosition.X += 1;
            robotPosition.Y += 1;
            robotPosition.Direction = Direction.North;

            var robotPositionAfterChanges = testRobot.Report();

            Assert.NotEqual(robotPositionAfterChanges.X, robotPosition.X);
            Assert.NotEqual(robotPositionAfterChanges.Y, robotPosition.Y);
            Assert.NotEqual(robotPositionAfterChanges.Direction, robotPosition.Direction);
        }

        [Fact]
        public void Report_WhenNotPlaced_ThrowsException()
        {
            var testGame = new ToyRobotGame();
            var testRobot = testGame.CreateRobot();

            Assert.Throws<RobotNotPlacedException>(() => testRobot.Report());
        }

        [Fact]
        public void Report_WhenReturnPosition_MakesDeepCopy()
        {
            var movementManagerMoq = new Mock<IMovementManager>();
            movementManagerMoq.Setup(_ => _.EnsureCanMove(It.IsAny<Point>()));

            var testRobot = new Robot(movementManagerMoq.Object);

            var robotPosition = new RobotPosition(1, 1, Direction.East);
            testRobot.Place(robotPosition);

            var placedRobotPosition = testRobot.Report();
            placedRobotPosition.X += 1;
            placedRobotPosition.Y += 1;
            placedRobotPosition.Direction = Direction.North;

            var robotPositionAfterChanges = testRobot.Report();

            Assert.Equal(robotPositionAfterChanges.X, robotPosition.X);
            Assert.Equal(robotPositionAfterChanges.Y, robotPosition.Y);
            Assert.Equal(robotPositionAfterChanges.Direction, robotPosition.Direction);
        }
    }
}
