﻿using System;
using ToyRobot.Game.Models;
using Xunit;

namespace ToyRobot.Game.Tests.Models
{
    public class RobotPositionTests
    {
        [Fact]
        public void RobotPosition_WithInvalidDirection_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => new RobotPosition(1, 1, (Direction)1000));
        }

        [Fact]
        public void RobotPosition_SetDirectionXY_Success()
        {
            uint expectedX = 5;
            uint expectedY = 7;
            Direction expectedDirection = Direction.North;

            var testRobotPosition = new RobotPosition(expectedX, expectedY, expectedDirection);

            Assert.Equal(testRobotPosition.X, expectedX);
            Assert.Equal(testRobotPosition.Y, expectedY);
            Assert.Equal(testRobotPosition.Direction, expectedDirection);
        }
    }
}
