﻿using System;
using ToyRobot.Game.Models;
using Xunit;

namespace ToyRobot.Game.Tests
{
    public class BoardTests
    {
        [Theory]
        [InlineData(2, 2, 1, 1, true)]
        [InlineData(2, 2, 2, 1, false)]
        [InlineData(2, 2, 1, 2, false)]
        [InlineData(2, 2, 2, 2, false)]
        public void IsValidPosition_WorksAsExpected(uint boardWidth, uint boardHeight, uint pointX, uint pointY, bool expectedValue)
        {
            var testBoard = new Board(boardWidth, boardHeight);
            var testPoint = new Point(pointX, pointY);
            Assert.Equal(testBoard.IsValidPosition(testPoint), expectedValue);
        }

        [Fact]
        public void IsValidPosition_WithNullPoint_ThrowsException()
        {
            var testBoard = new Board(5, 5);
            Assert.Throws<ArgumentNullException>(() => testBoard.IsValidPosition(null));
        }

        [Fact]
        public void Board_SetWidthHeight_Success()
        {
            uint expectedWidth = 5;
            uint expectedHeight = 7;
            var testBoard = new Board(expectedWidth, expectedHeight);
            Assert.Equal(testBoard.Width, expectedWidth);
            Assert.Equal(testBoard.Height, expectedHeight);
        }
    }
}
