﻿using ToyRobot.Game.Extensions;
using Xunit;

namespace ToyRobot.Game.Tests.Extensions
{
    public class DeepCopyExtensionTests
    {
        private class TestClass
        {
            public int TestInt { get; set; }
            public TestClass NestedTestClass { get; set; }
        }

        [Fact]
        public void DeepCopy_ClassWithNestedClass_Success()
        {
            var testObject = new TestClass()
            {
                TestInt = 10,
                NestedTestClass = new TestClass()
                {
                    TestInt = 5
                }
            };
            var testObjectCopy = testObject.DeepCopy();

            Assert.Equal(testObject.TestInt, testObjectCopy.TestInt);
            Assert.Equal(testObject.NestedTestClass.TestInt, testObjectCopy.NestedTestClass.TestInt);

            testObjectCopy.TestInt += 1;
            testObjectCopy.NestedTestClass.TestInt += 1;

            Assert.NotEqual(testObject.TestInt, testObjectCopy.TestInt);
            Assert.NotEqual(testObject.NestedTestClass.TestInt, testObjectCopy.NestedTestClass.TestInt);
        }

        [Fact]
        public void DeepCopy_WithNull_Success()
        {
            TestClass testObject = null;
            var testObjectCopy = testObject.DeepCopy();

            Assert.Null(testObjectCopy);
        }
    }
}
